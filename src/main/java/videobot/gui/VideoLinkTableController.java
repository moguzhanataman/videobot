package videobot.gui;

import videobot.core.Main;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.MenuItem;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.Clipboard;
import javafx.scene.input.ClipboardContent;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.web.WebView;
import javafx.stage.Stage;
import videobot.utils.Youtube;

import java.awt.*;
import java.io.IOException;
import java.net.URI;
import java.util.logging.Level;
import java.util.logging.Logger;

public class VideoLinkTableController {
    @FXML
    private TableView<VideoLink> tableView;
    @FXML
    private TableColumn<VideoLink, Integer> sender;
    @FXML
    private TableColumn<VideoLink, String> link;

    public ObservableList<VideoLink> links = FXCollections.observableArrayList();

    public void addLink(String sender, String link) {
        tableView.getItems().add(new VideoLink(sender, link));
    }

    @FXML
    protected void initialize() {
        sender.setCellValueFactory(new PropertyValueFactory<VideoLink, Integer>("sender"));
        link.setCellValueFactory(new PropertyValueFactory<VideoLink, String>("link"));

        tableView.setItems(links);

        ContextMenu rightClickMenu = rowRightClickMenu();

        tableView.setRowFactory(tv -> {
            TableRow<VideoLink> row = new TableRow<>();
            row.setOnMouseClicked(event -> {
                if (!row.isEmpty()) {
                    int clickCount = event.getClickCount();

                    if (event.getButton() == MouseButton.PRIMARY && clickCount == 2) {
                        VideoLink videoLink = row.getItem();
                        playYoutubeVideo(videoLink.getLink());
                    } else if (event.getButton() == MouseButton.SECONDARY) {
                        rightClickMenu.show(tableView, event.getScreenX(), event.getScreenY());
                    }
                }
            });
            return row;
        });
    }

    private void playYoutubeVideo(String link) {
        playYoutubeVideo(link, 0);
    }

    // Option 0 = embedded, 1 = full
    private void playYoutubeVideo(String link, int option) {
        Stage stage = new Stage();
        String finalUrl = null;
        WebView webview = new WebView();
        if (option == 2) {
            try {
                Desktop.getDesktop().browse(new URI(link));
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            if (option == 0) {
                finalUrl = Youtube.embeddedForm(link);
            } else if (option == 1) {
                finalUrl = Youtube.websiteForm(link);
            }
            System.out.println("Final url: " + finalUrl);
            webview.getEngine().load(finalUrl);
            webview.setPrefSize(640, 390);

            stage.setTitle(webview.getEngine().getTitle());
            stage.setScene(new Scene(webview, 450, 450));

            stage.show();
            stage.setOnCloseRequest(event -> {
                webview.getEngine().load(null);
            });
        }
    }

    public void botSetupClicked(MouseEvent mouseEvent) {
        try {
            FXMLLoader fxmlLoader = new FXMLLoader();
            fxmlLoader.setLocation(getClass().getResource("/fxml/BotSetup.fxml"));

            Scene scene = new Scene(fxmlLoader.load(), 600, 400);
            Stage stage = new Stage();
            stage.setTitle("New Window");
            stage.setScene(scene);
            stage.show();
        } catch (IOException e) {
            Logger logger = Logger.getLogger(getClass().getName());
            logger.log(Level.SEVERE, "Failed to create new Window.", e);
        }
    }

    public void removeLink(MouseEvent mouseEvent) {
        ObservableList<VideoLink> selectedItems = tableView.getSelectionModel().getSelectedItems();
        if (selectedItems != null) {
            tableView.getItems().removeAll(selectedItems);
        }
    }

    public void clearVideoLinks(MouseEvent mouseEvent) {
        Main.bot.clearVotes();
        links.clear();
        tableView.getItems().clear();
    }

    private ContextMenu rowRightClickMenu() {
        ContextMenu cm = new ContextMenu();

        MenuItem mi1 = new MenuItem("Embedded player ile aç");
        mi1.setOnAction(event -> {
            playYoutubeVideo(tableView.getSelectionModel().getSelectedItem().getLink());
        });
        MenuItem mi2 = new MenuItem("Site görünümünde aç");
        mi2.setOnAction(event -> {
            playYoutubeVideo(tableView.getSelectionModel().getSelectedItem().getLink(), 1);
        });
        MenuItem mi3 = new MenuItem("Varsayılan tarayıcıda aç");
        mi3.setOnAction(event -> {
            playYoutubeVideo(tableView.getSelectionModel().getSelectedItem().getLink(), 2);
        });
        MenuItem mi4 = new MenuItem("Bağlantı adresini kopyala");
        mi4.setOnAction(event -> {
            ClipboardContent content = new ClipboardContent();
            content.putString(tableView.getSelectionModel().getSelectedItem().getLink());
            Clipboard.getSystemClipboard().setContent(content);
        });
        MenuItem mi5 = new MenuItem("Sil");
        mi5.setOnAction(event -> {
            tableView.getItems().remove(tableView.getSelectionModel().getSelectedItem());
        });

        cm.getItems().addAll(mi1, mi2, mi3, mi4, mi5);

        return cm;
    }
}
