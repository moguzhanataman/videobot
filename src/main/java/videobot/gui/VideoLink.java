package videobot.gui;

import javafx.beans.property.SimpleStringProperty;

public class VideoLink {
    private final SimpleStringProperty sender;
    private final SimpleStringProperty link;

    public VideoLink(String sender, String link) {
        this.sender = new SimpleStringProperty(sender);
        this.link = new SimpleStringProperty(link);
    }

    public String getSender() {
        return sender.get();
    }

    public SimpleStringProperty senderProperty() {
        return sender;
    }

    public void setSender(String sender) {
        this.sender.set(sender);
    }

    public String getLink() {
        return link.get();
    }

    public SimpleStringProperty linkProperty() {
        return link;
    }

    public void setLink(String link) {
        this.link.set(link);
    }
}
