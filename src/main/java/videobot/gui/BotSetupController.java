package videobot.gui;

import videobot.core.Config;
import videobot.core.Main;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;

import java.io.*;
import java.util.Properties;

public class BotSetupController {
    public static final String CONFIG_FILE_NAME = "botConfig.properties";
    Properties prop = new Properties();
    InputStream input = null;
    OutputStream output = null;

    @FXML
    public TextField botName;
    @FXML
    public PasswordField oauthToken;
    @FXML
    public TextField channel;
    @FXML
    public ComboBox mode;
    @FXML
    public Button saveButton;

    @FXML
    protected void initialize() {
        if (Config.notLoaded) {
            Config.loadConfig();
        }
        botName.setText(Config.botName);
        oauthToken.setText(Config.oauthToken);
        channel.setText(Config.channel);
        mode.setValue(Config.mode);
    }

    public void saveConfig(MouseEvent mouseEvent) {
        Config.saveConfig(botName.getText(), oauthToken.getText(), channel.getText(), mode.getValue().toString());
        Main.bot.stopBot();
        Main.bot.configureAndStartBot();
        Stage stage = (Stage) saveButton.getScene().getWindow();
        stage.close();
    }
}
