package videobot.gui;

import videobot.core.Main;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.IOException;

public class GUI extends Application {
    public static VideoLinkTableController videoLinkTableController = null;
    public static Stage mainStage = null;
    public static Parent root;
    public static void startGUI(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage stage) {
        try {
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(getClass().getResource("/fxml/VideoLinkTable.fxml"));
            root = loader.load();
            videoLinkTableController = loader.getController();
            stage.setScene(new Scene(root));
            stage.setMinWidth(400.0);
            stage.setMinHeight(600.0);
            stage.setTitle("Video Bot");
            stage.setOnCloseRequest(event -> {
                Main.bot.stopBot();
            });
            stage.show();
            mainStage = stage;
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


}