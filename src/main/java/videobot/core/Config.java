package videobot.core;

import java.io.*;
import java.util.Properties;

public class Config {
    public static final String FILE_NAME = "botConfig.properties";

    public static Properties prop = new Properties();
    public static InputStream input = null;
    public static OutputStream output = null;

    public static String botName = "";
    public static String oauthToken = "";
    public static String channel = "";
    public static String mode = "";

    public static boolean notLoaded = true;

    public static void loadConfig() {
        try {
            FileInputStream input = new FileInputStream(FILE_NAME);
            prop.load(input);
        } catch (java.io.IOException e) {
            e.printStackTrace();
        } finally {
            closeStream(input);
        }

        botName = prop.getProperty("botName");
        oauthToken = prop.getProperty("oauthToken");
        channel = prop.getProperty("channel");
        mode = prop.getProperty("mod");

        if (botName == null) botName = "";
        if (oauthToken == null) oauthToken = "";
        if (channel == null) channel = "";
        if (mode == null) mode = "";

        notLoaded = false;
    }

    public static void saveConfig(String _botName, String _oauthToken, String _channel, String _mode) {
        botName = _botName;
        oauthToken = _oauthToken;
        channel = _channel;
        mode = _mode;

        if (botName == null) botName = "";
        if (oauthToken == null) oauthToken = "";
        if (channel == null) channel = "";
        if (mode == null) mode = "";

        prop.setProperty("botName", botName);
        prop.setProperty("oauthToken", oauthToken);
        prop.setProperty("channel", channel);
        prop.setProperty("mode", mode);

        try {
            output = new FileOutputStream(FILE_NAME);
            prop.store(output, null);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeStream(output);
        }
    }

    private static void closeStream(Closeable stream) {
        if (stream != null) {
            try {
                stream.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
