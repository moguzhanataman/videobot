package videobot.core;

import videobot.bot.Bot;
import videobot.gui.GUI;

public class Main {

	public static Bot bot = new Bot();

	public static void main(String[] args) {
        Config.loadConfig();
		bot.configureAndStartBot();

		GUI.startGUI(args);
	}
}