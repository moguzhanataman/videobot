package videobot.utils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Youtube {

    public static Pattern pattern = Pattern.compile(
            "^((?:https?:)?\\/\\/)?((?:www|m)\\.)?((?:youtube\\.com|youtu.be))(\\/(?:[\\w\\-]+\\?v=|embed\\/|v\\/)?)([\\w\\-]+)(\\S+)?$",
            Pattern.CASE_INSENSITIVE);

    public static String extractLink(String url) {
        String vId = null;
        Matcher matcher = pattern.matcher(url);
        if (matcher.matches()) {
            vId = matcher.group(0);
        }
        return vId;
    }

    public static String embeddedForm(String url) {
        Matcher matcher = pattern.matcher(url);
        if (matcher.matches()) {
            return "https://www.youtube.com/embed/" + matcher.group(5) + "?autoplay=1";
        }

        return null;
    }

    public static String websiteForm(String url) {
        Matcher matcher = pattern.matcher(url);
        if (matcher.matches()) {
            return "https://www.youtube.com/watch?v=" + matcher.group(5) + "&autoplay=1";
        }

        return null;
    }
}
