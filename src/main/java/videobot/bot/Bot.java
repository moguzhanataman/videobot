package videobot.bot;

import com.google.common.collect.HashMultiset;
import com.google.common.collect.ImmutableMultiset;
import com.google.common.collect.Multiset;
import com.google.common.collect.Multisets;

import videobot.core.Config;
import videobot.gui.GUI;
import org.pircbotx.Configuration;
import org.pircbotx.PircBotX;
import org.pircbotx.hooks.ListenerAdapter;
import org.pircbotx.hooks.events.PingEvent;
import org.pircbotx.hooks.types.GenericMessageEvent;
import videobot.utils.Youtube;

import java.util.*;

public class Bot extends ListenerAdapter {

    public static PircBotX pircBotx;
    public static Thread botThread;

    //Mods usernames must be lowercase
    private List<String> mods = new ArrayList<>();
    private List<GenericMessageEvent> messages = new ArrayList<>();
    private Multiset<String> wordSet = HashMultiset.create();

    public Bot() {
    }

    public void clearVotes() {
        messages.clear();
        wordSet.clear();
    }

    public ImmutableMultiset<String> aggregateLinks() {
        List<String> msgList = new ArrayList<>();
        messages.forEach(m -> {
            String message = m.getMessage();
            String[] msgs = message.split(" ");
            msgList.addAll(Arrays.asList(msgs));
        });
        wordSet.addAll(msgList);
        messages.clear(); // we don't clear wordSet because successive calls to this function must accumulate wordSet but must not cause duplication
        ImmutableMultiset<String> sortedWordSet = Multisets.copyHighestCountFirst(wordSet);
        return sortedWordSet;
    }

    /**
     * PircBotx will return the exact message sent and not the raw line
     */
    @Override
    public void onGenericMessage(GenericMessageEvent event) throws Exception {
        String message = event.getMessage();
        String sender = event.getUser().getNick();
        String linkOrNull = Youtube.extractLink(message);
        System.out.println("msg: " + message);

        if (linkOrNull != null) {
            messages.add(event);
            GUI.videoLinkTableController.addLink(sender, linkOrNull);
        }

    }

    /**
     * We MUST respond to this or else we will get kicked
     */
    @Override
    public void onPing(PingEvent event) throws Exception {
        pircBotx.sendRaw().rawLineNow(String.format("PONG %s\r\n", event.getPingValue()));
    }

    private void sendMessage(String message) {
        if (!Config.mode.equals("listen")) {
            pircBotx.sendIRC().message("#" + Config.channel, message);
        }
    }

    public void configureAndStartBot() {
        botThread = new Thread(() -> {
            Configuration config = new Configuration.Builder()
                    .setName(Config.botName)
                    .setServer("irc.chat.twitch.tv", 6667)
                    .setServerPassword(Config.oauthToken)
                    .addListener(this)
                    .addAutoJoinChannel("#" + Config.channel)
                    .buildConfiguration();

            pircBotx = new PircBotX(config);
            try {
                pircBotx.startBot();
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
        botThread.start();
    }

    public void stopBot() {
        if (pircBotx == null) return;

        if (pircBotx.isConnected()) {
            pircBotx.stopBotReconnect();
            pircBotx.sendIRC().quitServer("bye");
        }
    }
}